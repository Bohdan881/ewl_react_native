/**
 * @format
 */
import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {firebase} from './src/firebase/config';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  LoginScreen,
  RegistrationScreen,
  ForgetPasswordScreen,
} from './src/screens';
import {setUserSessionValues} from './src/redux/actions';
import AuthorizedUserStackScreen from './src/screens/AuthorizedUserStackScreen';
import LessonContentScreen from './src/screens/LessonContentScreen';
import TopicListScreen from './src/screens/TopicListScreen';

const Stack = createStackNavigator();
const RootStack = createStackNavigator();

// TO DO
// set up orientation
const App = () => {
  const {activeLesson} = useSelector((db) => db.userSessionValues);
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(true);
  const [isUserAuthorized, setUserAuthorization] = useState(false);

  useEffect(() => {
    firebase.auth().onAuthStateChanged((authUser) => {
      if (authUser) {
        const uid = authUser.uid;

        // fetch user data by uid
        firebase
          .database()
          .ref(`users/${uid}`)
          .once('value')
          .then((snapshot) => {
            const {
              email,
              emailVerified,
              isAgreedOnCookies,
              username,
              lessonsInProgress,
              lessonsCompleted,
              signDate,
            } = snapshot.val() || {};

            dispatch(
              setUserSessionValues({
                uid,
                email,
                userName: username,
                isEmailVerified: emailVerified,
                isAgreedOnCookies,
                lessonsInProgress,
                lessonsCompleted,
                signDate,
              }),
            );

            setUserAuthorization(true);
            setLoading(false);
          })
          .catch((error) => {
            setLoading(false);
            alert(error);
          });
      } else {
        setLoading(false);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading) {
    return <></>;
  }

  return (
    <NavigationContainer>
      <>
        <RootStack.Navigator>
          {isUserAuthorized && (
            <>
              <RootStack.Screen
                name="Main"
                component={AuthorizedUserStackScreen}
                options={{headerShown: false}}
              />
              <Stack.Screen
                name="LessonContent"
                component={LessonContentScreen}
                options={{title: activeLesson || 'Lesson'}}
              />
            </>
          )}

          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Registration" component={RegistrationScreen} />
          <Stack.Screen
            name="Forget Password"
            component={ForgetPasswordScreen}
          />
          <Stack.Screen name="Topic List" component={TopicListScreen} />
        </RootStack.Navigator>
      </>
    </NavigationContainer>
  );
};

export default App;
