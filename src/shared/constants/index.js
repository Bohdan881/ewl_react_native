/*
  Constants that are shared acccros the app
*/

export const LESSON_TYPES = [
  {label: 'Video', icon: 'video-vintage'},
  {label: 'Vocabulary', icon: 'bookshelf'},
  {label: 'Phrasal Verbs', icon: 'typewriter'},
];

export const CHAPTER_ORDER_BY_LESSON_TYPE = {
  Video: [
    'About',
    // 'Words in context',
    // 'Fill in the blanks',
    // 'Definitions',
    'Another way to say',
    // 'Writing Practice',
    'Content',
    'Speaking',
  ],
  Vocabulary: [
    'About',
    'New Words',
    // 'Words in context',
    // 'Fill in the blanks',
    // 'Definitions',
    'Another way to say',
    // 'Cloze',
    // 'Writing Practice',
    'Speaking',
  ],
  'Phrasal Verbs': [
    'About',
    'Phrasal Verbs',
    // 'Words in context',
    // 'Fill in the blanks',
    // 'Definitions',
    'Another way to say',
    // 'Cloze',
    // 'Writing Practice',
    'Speaking',
  ],
};
