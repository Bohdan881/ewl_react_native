// shared app colours

export const WHITE = '#fff';
export const CORN_FLOWER_BLUE = '#788eec';
export const DARK_GREY = '#2E2E2D';
export const LIGHT_GREY = '#CCCCCC';
export const FACEBOOK_COLOUR = '#3b5998';
export const GRAY = '#aaaaaa';
export const VIVID_ORANGE = '#ffb70a';
export const SUCCESS_GREEN = '#4DAD4A';
export const SELAGO = '#E8E6E8';
export const RED = '#C94131';
export const LIGHT_RED = '#ffcccb';
