const asyncActionType = (type) => ({
  PENDING: `${type}_PENDING`,
  SUCCESS: `${type}_SUCCESS`,
  ERROR: `${type}_ERROR`,
});

/* API */
export const API_REQUEST = 'API_REQUEST';

/* API requests  */
export const GET_LESSONS = asyncActionType('GET_LESSONS');

export const SET_USER_SESSION_VALUES = 'SET_USER_SESSION_VALUES';
