import {createStore, compose, applyMiddleware} from 'redux';
import firebasePostsMiddleware from './middleware';
import rootReducer from './reducers/root';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(firebasePostsMiddleware)),
);

window.store = store; // makes the store available globally
