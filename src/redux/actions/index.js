import {
  API_REQUEST,
  GET_LESSONS,
  SET_USER_SESSION_VALUES,
} from '../constants/actionTypes';

export const setUserSessionValues = (values) => ({
  type: SET_USER_SESSION_VALUES,
  payload: values,
});

export const getLessons = (values) => ({
  type: API_REQUEST,
  payload: values,
  next: GET_LESSONS,
});
