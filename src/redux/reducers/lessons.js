import {GET_LESSONS, LESSONS_GET} from '../constants/actionTypes';

const initState = {
  loading: false,
  error: false,
  errorMessage: null,
  data: null,
};

const lessonsReducer = (state = initState, action) => {
  switch (action.type) {
    case GET_LESSONS.PENDING:
      return {
        ...state,
        loading: true,
      };
    case GET_LESSONS.ERROR:
      return {
        ...state,
        error: true,
        errorMessage: action.payload,
        data: [],
        loading: false,
      };
    case GET_LESSONS.SUCCESS:
      return {
        ...state,
        data: action.payload,
        error: false,
        errorMessage: null,
        loading: false,
      };
    default:
      return state;
  }
};

export default lessonsReducer;
