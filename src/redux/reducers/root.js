import {combineReducers} from 'redux';
import lessonsReducer from './lessons';
import userSessionValuesReducer from './userSession';

const rootReducer = combineReducers({
  lessons: lessonsReducer,
  userSessionValues: userSessionValuesReducer,
});

export default rootReducer;
