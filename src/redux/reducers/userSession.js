import {SET_USER_SESSION_VALUES} from '../constants/actionTypes';

const initState = {
  uid: null,
  email: '',
  userName: null,
  isEmailVerified: false,
  isAgreedOnCookies: false,
  lessonsInProgress: null,
  activeLesson: null,
};

const userSessionValuesReducer = (state = initState, action) => {
  const {type, payload} = action;

  switch (type) {
    case SET_USER_SESSION_VALUES:
      return {
        ...state,
        ...payload,
      };
    default:
      return state;
  }
};

export default userSessionValuesReducer;
