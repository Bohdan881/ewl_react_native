import {API_REQUEST} from '../constants/actionTypes';
import {firebase} from '../../firebase/config';

const firebasePostsMiddleware = (store) => (next) => (action) => {
  if (action.type === API_REQUEST) {
    next({
      type: action.next.PENDING,
    });

    firebase
      .database()
      .ref('posts')
      .on(
        'value',
        (snapshot) => {
          const lessonsObject = snapshot && snapshot.val();
          if (lessonsObject) {
            let response = {};
            Object.entries(lessonsObject || {}).forEach(
              ([key, lessonContent]) => {
                const {category, subCategory} = lessonContent;

                if (!response[category]) {
                  response[category] = {[subCategory]: [lessonContent]};
                } else {
                  if (!response[category][subCategory]) {
                    response[category][subCategory] = [lessonContent];
                  } else {
                    response[category][subCategory] = [
                      ...response[category][subCategory],
                      lessonContent,
                    ];
                  }
                }
              },
            );

            next({
              type: action.next.SUCCESS,
              payload: response,
            });
          }
        },
        (error) => {
          next({
            type: action.next.ERROR,
            error: true,
            payload: error.message,
          });
        },
      );
  } else {
    next(action);
  }
};

export default firebasePostsMiddleware;
