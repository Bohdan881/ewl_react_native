import * as firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';
import {
  REACT_APP_API_KEY,
  REACT_APP_AUTH_DOMAIN,
  REACT_APP_DATABASE_URL,
  REACT_APP_PROJECT_ID,
  REACT_APP_ID,
  REACT_APP_STORAGE_BUCKET,
  REACT_APP_MESSAGING_SENDER_ID,
  REACT_APP_CONFIRMATION_EMAIL_REDIRECT,
} from '@env';

/*  The env global variable is injected by the Node
    at runtime for your application to use and it represents
    the state of the system environment your application is
    in when it starts.
*/

const firebaseConfig = {
  apiKey: REACT_APP_API_KEY,
  authDomain: REACT_APP_AUTH_DOMAIN,
  databaseURL: 'https://in-english-with-love.firebaseio.com',
  projectId: REACT_APP_PROJECT_ID,
  appId: REACT_APP_ID,
  storageBucket: REACT_APP_STORAGE_BUCKET,
  messagingSenderId: REACT_APP_MESSAGING_SENDER_ID,
  confirmationRedirect: REACT_APP_CONFIRMATION_EMAIL_REDIRECT,
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export {firebase};
