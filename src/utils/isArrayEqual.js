/**
 * compare if arrays of objects are equal to each other
 */

import _ from 'lodash';

const isArrayEqual = function (x, y) {
  return _(x).xorWith(y, _.isEqual).isEmpty();
};

export default isArrayEqual;
