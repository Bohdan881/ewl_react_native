import React from 'react';
import {Card, CardItem, Icon, Left, Right, Text, View} from 'native-base';
import {Image} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {convertMillisecondsToDate} from '../../utils';

// style
import styles from './style';
import {setUserSessionValues} from '../../redux/actions';

const TopicListScreen = ({navigation, route}) => {
  const {type, category} = route.params;
  const {lessons, userSessionValues} = useSelector((db) => db);
  const {lessonsCompleted = {}, lessonsInProgress = {}} =
    userSessionValues || {};

  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
        {lessons.data[type][category].map((obj) => {
          const {uid, title, date, iconPath} = obj;
          const isLessonCompleted =
            Object.entries(lessonsCompleted).length > 0 &&
            !!Object.keys(lessonsCompleted).filter((completedLessonId) =>
              completedLessonId.includes(uid),
            ).length;

          return (
            <Card
              style={styles.cardContainer}
              key={uid}
              onTouchStart={() => {
                navigation.navigate('LessonContent', {
                  ...obj,
                  isLessonCompleted,
                });

                dispatch(
                  setUserSessionValues({
                    activeLesson: title,
                  }),
                );
              }}>
              <CardItem cardBody>
                <Left>
                  <View style={styles.cardInfoContainer}>
                    <Text style={styles.cardHeader}>{title}</Text>
                    <View style={styles.cardDateContainer}>
                      <Text style={styles.cardDate}>
                        {convertMillisecondsToDate(date)}
                      </Text>
                      <Icon
                        name="star"
                        style={[
                          styles.cardStarIcon,
                          isLessonCompleted
                            ? styles.lessonCompletedIcon
                            : styles.lessonInCompleteIcon,
                        ]}
                      />
                    </View>
                  </View>
                </Left>
                <Right>
                  <Image source={{uri: iconPath}} style={styles.cardImage} />
                </Right>
              </CardItem>
            </Card>
          );
        })}
      </KeyboardAwareScrollView>
    </View>
  );
};

export default TopicListScreen;
