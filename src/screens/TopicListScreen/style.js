import {StyleSheet, Dimensions} from 'react-native';
import {VIVID_ORANGE} from '../../shared/styles/colours';
const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: height * 0.01,
  },
  cardContainer: {
    height: 120,
    width: width * 0.9,
    borderColor: 'transparent',
  },
  cardInfoContainer: {
    padding: 15,
    height: 120,
    justifyContent: 'space-between',
    width: '110%',
  },
  cardHeader: {
    fontSize: 16,
    fontWeight: '500',
  },
  cardImage: {
    width: 100,
    margin: 10,
    height: 100,
    borderRadius: 2,
  },
  cardDateContainer: {
    display: 'flex',
    flexDirection: 'row',
  },
  cardStarIcon: {
    fontSize: 10,
    position: 'relative',
    top: 3,
    left: 2,
  },
  cardDate: {
    fontSize: 14,
    opacity: 0.7,
  },
  lessonInCompleteIcon: {
    opacity: 0.4,
  },
  lessonCompletedIcon: {
    color: VIVID_ORANGE,
  },
});
