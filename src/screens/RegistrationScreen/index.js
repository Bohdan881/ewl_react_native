import React, {useState} from 'react';
import {Image, Text, View} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import {GRAY} from '../../shared/styles/colours';
import {firebase} from '../../firebase/config';

// style
import style from './style';

const RegistrationScreen = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const onFooterLinkPress = () => {
    navigation.navigate('Login');
  };

  const onRegisterPress = () => {
    // check username and password match
    if (username.length < 4) {
      alert('The username must be 4 characters long or more.');
    }
    if (password !== confirmPassword) {
      alert("Passwords don't match.");
    }
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((response) => {
        const uid = response.user.uid;
        firebase
          .database()
          .ref(`users/${uid}`)
          .set({
            uid,
            email,
            signDate: new Date().getTime(),
            username,
            isEmailVerified: false,
            isAgreedOnCookies: false,
            lessonProgress: {},
            lessonsCompleted: {},
          })
          .then(() => {
            firebase
              .auth()
              .currentUser.sendEmailVerification({
                url: 'https://in-english-with-love.web.app/account',
              })
              .then(() => navigation.navigate('Main'))
              .catch((error) => alert(error));
          })
          .catch((error) => alert(error));
      })
      .catch((error) => alert(error));
  };

  const onRegisterWithFacebookPress = () => {};

  const onRegisterWithGooglePress = () => {};

  return (
    <View style={style.container}>
      <KeyboardAwareScrollView
        style={style.scrollView}
        keyboardShouldPersistTaps="always">
        <Text style={style.createAccountHeader}>Create Account</Text>
        <Image
          style={style.logo}
          source={require('../../assets/default.png')}
        />
        <TextInput
          style={style.input}
          placeholder="Username"
          placeholderTextColor={GRAY}
          onChangeText={(text) => setUsername(text)}
          value={username}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TextInput
          style={style.input}
          placeholder="E-mail"
          placeholderTextColor={GRAY}
          onChangeText={(text) => setEmail(text)}
          value={email}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TextInput
          style={style.input}
          placeholderTextColor={GRAY}
          secureTextEntry
          placeholder="Password"
          onChangeText={(text) => setPassword(text)}
          value={password}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TextInput
          style={style.input}
          placeholderTextColor="#aaaaaa"
          secureTextEntry
          placeholder="Confirm Password"
          onChangeText={(text) => setConfirmPassword(text)}
          value={confirmPassword}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TouchableOpacity
          style={style.button}
          onPress={() => onRegisterPress()}>
          <Text style={style.buttonTitle}>Create account</Text>
        </TouchableOpacity>

        <View style={style.footerView}>
          <View style={style.footerLineView}>
            <View style={style.footerLine} />
            <View>
              <Text style={style.footerLineText}>OR</Text>
            </View>
            <View style={style.footerLine} />
          </View>
          <View style={style.buttonWrapper}>
            <Icon.Button
              name="facebook"
              style={style.facebookButton}
              onPress={() => onRegisterWithFacebookPress()}>
              Sign up with Facebook
            </Icon.Button>
          </View>
          <View style={style.buttonWrapper}>
            <Icon.Button
              name="google"
              style={style.googleButton}
              onPress={() => onRegisterWithGooglePress()}>
              Sign up with Google
            </Icon.Button>
          </View>

          <Text style={style.footerText}>
            Already got an account?{' '}
            <Text onPress={onFooterLinkPress} style={style.footerLink}>
              Log in
            </Text>
          </Text>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default RegistrationScreen;
