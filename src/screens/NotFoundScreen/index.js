import React from 'react';
import {View, Text} from 'native-base';
import {SafeAreaView} from 'react-native-safe-area-context';

// styles
import styles from './style';

const NotFoundScreen = ({message}) => {
  return (
    <View style={[styles.container, styles.horizontal]}>
      <SafeAreaView>
        <Text style={styles.textContent}>
          {message || "The data doesn't exists"}
        </Text>
      </SafeAreaView>
    </View>
  );
};

export default NotFoundScreen;
