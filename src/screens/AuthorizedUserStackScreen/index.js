import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {LESSON_TYPES} from '../../shared/constants';
import LessonTypeScreen from '../LessonTypeScreen';
import UserProfileScreen from '../UserProfileScreen';

const Tab = createBottomTabNavigator();

const AuthorizedUserStackScreen = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Profile"
        component={UserProfileScreen}
        options={{
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="account" color={color} size={size} />
          ),
        }}
      />
      {LESSON_TYPES.map(({label, icon}) => (
        <Tab.Screen
          key={label}
          name={label}
          options={{
            tabBarIcon: ({color, size}) => (
              <MaterialCommunityIcons name={icon} color={color} size={size} />
            ),
          }}>
          {(props) => <LessonTypeScreen {...props} type={label} />}
        </Tab.Screen>
      ))}
    </Tab.Navigator>
  );
};

export default AuthorizedUserStackScreen;
