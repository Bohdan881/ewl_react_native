import React from 'react';
import {View} from 'native-base';
import {ActivityIndicator} from 'react-native';

// styles
import styles from './style';

const LoadingScreen = () => (
  <View style={[styles.container, styles.horizontal]}>
    <ActivityIndicator size="large" />
  </View>
);

export default LoadingScreen;
