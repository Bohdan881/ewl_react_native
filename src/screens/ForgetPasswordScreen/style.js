import {StyleSheet} from 'react-native';
import {Dimensions} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
const {width} = Dimensions.get('window');
import {CORN_FLOWER_BLUE, RED, WHITE} from '../../shared/styles/colours';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    margin: width * 0.05,
    marginTop: heightPercentageToDP('20%'),
  },
  scrollView: {
    flex: 1,
    width: '100%',
  },
  header: {
    margin: 10,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  input: {
    height: width * 0.1,
    borderRadius: 5,
    overflow: 'hidden',
    backgroundColor: WHITE,
    marginTop: 10,
    marginBottom: 10,
    paddingLeft: 16,
  },
  button: {
    backgroundColor: CORN_FLOWER_BLUE,
    marginTop: 10,
    height: width * 0.11,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonCancel: {
    backgroundColor: RED,
    marginTop: 10,
    height: width * 0.11,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonTitle: {
    color: WHITE,
    fontSize: 16,
    fontWeight: 'bold',
  },
});
