import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {firebase} from '../../firebase/config';
import {GRAY} from '../../shared/styles/colours';

// style
import style from './style';

const ForgetPasswordScreen = ({navigation}) => {
  const [email, setEmail] = useState('');

  const onResetPasswordLink = () =>
    firebase.auth().sendPasswordResetEmail(email);

  return (
    <View style={style.container}>
      <KeyboardAwareScrollView style={style.scrollView}>
        <Text style={style.header}>Reset your password</Text>
        <TextInput
          style={style.input}
          placeholder="E-mail"
          placeholderTextColor={GRAY}
          onChangeText={(text) => setEmail(text)}
          value={email}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TouchableOpacity
          style={style.button}
          onPress={() =>
            onResetPasswordLink()
              .then(() =>
                alert(
                  'Please check your E-mail (Spam folder included) for confirmation.',
                ),
              )
              .catch((error) => alert(error))
          }>
          <Text style={style.buttonTitle}>Reset my password</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={style.buttonCancel}
          onPress={() => navigation.navigate('Login')}>
          <Text style={style.buttonTitle}>Cancel</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default ForgetPasswordScreen;
