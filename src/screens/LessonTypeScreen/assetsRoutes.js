/* match subCategory with its own asset */
import Music from '../../assets/music2.png';
import Grammar from '../../assets/grammar.png';
import Vocabulary from '../../assets/grammar.png';
import Travel from '../../assets/travel.png';
import UnusualActivities from '../../assets/travel.png';
import Movies from '../../assets/movies.png';
import MindBody from '../../assets/mind_body.png';
import Philosophy from '../../assets/philosophy.png';
import Sustainability from '../../assets/sustainability.png';
import ArtAndCulture from '../../assets/art_culture.png';
import SocialIssues from '../../assets/social_issues.png';
import NatureAndAnimals from '../../assets/nature.png';

export const assetsRoutes = {
  Music,
  Grammar,
  Vocabulary,
  Travel,
  Movies,
  Sustainability,
  Philosophy,
  'Mind & Body': MindBody,
  'Art & Culture': ArtAndCulture,
  'Unusual Activities': UnusualActivities,
  'Social Issues': SocialIssues,
  'Nature & Animals': NatureAndAnimals,
};
