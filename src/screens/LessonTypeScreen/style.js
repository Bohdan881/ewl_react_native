import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: height * 0.06,
    margin: 1,
  },

  cardContainer: {
    height: 120,
    width: width * 0.9,
    borderRadius: 4,
    marginTop: 15,
  },
  cardImage: {
    width: 90,
    height: 100,
    marginLeft: 20,
    marginTop: 10,
  },
  cardCategoryInfoView: {
    marginLeft: 15,
    height: 60,
    justifyContent: 'space-between',
  },
  cardCategoryHeader: {
    fontSize: 18,
  },

  cardLessonsQuantityView: {
    flexDirection: 'row',
    textAlign: 'left',
    opacity: 0.5,
  },
});
