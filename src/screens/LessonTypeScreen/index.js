import React, {useEffect} from 'react';
import {View, Text, Card, CardItem} from 'native-base';
import {Image} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useDispatch, useSelector} from 'react-redux';
import {getLessons} from '../../redux/actions';
import LoadingScreen from '../LoadingScreen';
import NotFoundScreen from '../NotFoundScreen';

// styles
import styles from './style';

// assets
import {assetsRoutes} from './assetsRoutes';

const LessonTypeScreen = ({type, navigation}) => {
  const {data, error, errorMessage, loading} = useSelector((db) => db.lessons);
  const dispatch = useDispatch();

  useEffect(() => {
    if (data === null) {
      dispatch(getLessons());
    }
  }, [dispatch, data]);

  return error ? (
    <NotFoundScreen message={errorMessage} />
  ) : loading ? (
    <LoadingScreen />
  ) : data && Object.entries(data[type] || {}).length === 0 ? (
    <NotFoundScreen
      message={`Lessons don't in the ${type || 'current'} category. `}
    />
  ) : (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        style={styles.scrollView}
        keyboardShouldPersistTaps="always">
        {data &&
          Object.keys(data[type] || {}).map((category) => (
            <Card
              style={styles.cardContainer}
              key={category}
              onTouchStart={() =>
                navigation.navigate('Topic List', {category, type})
              }>
              <CardItem cardBody>
                <Image
                  style={styles.cardImage}
                  source={
                    assetsRoutes[category] ||
                    require('../../assets/default.png')
                  }
                />
                <View style={styles.cardCategoryInfoView}>
                  <Text style={styles.cardCategoryHeader}>{category}</Text>
                  <View style={styles.cardLessonsQuantityView}>
                    <Text style={styles.cardLessonsQuantityText}>
                      {data[type][category].length}{' '}
                      {`${
                        data[type][category].length === 1 ? 'lesson' : 'lessons'
                      }`}
                    </Text>
                  </View>
                </View>
              </CardItem>
            </Card>
          ))}
      </KeyboardAwareScrollView>
    </View>
  );
};

export default LessonTypeScreen;
