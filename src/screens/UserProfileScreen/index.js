import {Content, Form, Input, Item} from 'native-base';
import React from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useSelector} from 'react-redux';

// styles
import styles from './style';

const UserProfileScreen = ({navigation}) => {
  const {email, lessonsCompleted, userName} = useSelector(
    (db) => db.userSessionValues,
  );

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        style={styles.scrollView}
        keyboardShouldPersistTaps="always">
        <SafeAreaView>
          <Text style={styles.header}> Overview</Text>
          <View style={styles.userInfoContainer}>
            <View style={styles.userInfoCredentialField}>
              <Icon style={styles.userInfoCredentialIcon} name="user" />
              <Text style={styles.userInfoCredentialTitle}> Username: </Text>
              <Text style={styles.userCredentialText}>
                {' '}
                {userName || 'User'}
              </Text>
            </View>
            <View style={styles.userInfoCredentialField}>
              <Icon style={styles.userInfoCredentialIcon} name="folder" />
              <Text style={styles.userInfoCredentialTitle}> Email: </Text>
              <Text style={styles.userCredentialText}>
                {' '}
                {email || 'user@gmail.com'}
              </Text>
            </View>
            <View style={styles.userInfoCredentialField}>
              <Icon style={styles.userInfoCredentialIcon} name="mortar-board" />
              <Text style={styles.userInfoCredentialTitle}>
                Completed Lessons:
              </Text>
              <Text style={styles.userCredentialText}>
                {' '}
                {Object.keys(lessonsCompleted || {}).length}
              </Text>
            </View>
            <View style={styles.buttonWrapper}>
              <Icon.Button
                name="sign-out"
                backgroundColor="transparent"
                color="black"
                style={styles.logoutButton}
                onPress={() => navigation.navigate('Login')}>
                Logout
              </Icon.Button>
            </View>
          </View>
        </SafeAreaView>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default UserProfileScreen;
