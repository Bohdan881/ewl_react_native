import {StyleSheet, Dimensions} from 'react-native';
const {width} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    margin: 10,
  },
  scrollView: {
    flex: 1,
    width: '100%',
  },
  header: {
    paddingTop: 15,
    fontSize: 20,
    alignSelf: 'center',
    fontWeight: '600',
  },
  userInfoContainer: {
    paddingTop: 30,
  },

  userInfoCredentialField: {
    flexDirection: 'row',
    paddingBottom: 15,
  },
  userInfoCredentialIcon: {
    fontSize: 18,
    position: 'relative',
    top: 2,
  },
  userInfoCredentialTitle: {
    fontSize: 18,
  },
  userCredentialText: {
    fontSize: 18,
    fontWeight: '600',
  },
  buttonWrapper: {
    width: 100,
  },
  logoutButton: {
    width: 300,
    backgroundColor: 'transparent',
    opacity: 0.7,
    position: 'relative',
    left: -5,
  },
});
