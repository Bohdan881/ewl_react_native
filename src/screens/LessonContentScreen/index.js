import React, {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {View, Text, Button} from 'native-base';
import ProgressBar from 'react-native-progress/Bar';
import {Dimensions} from 'react-native';
import {CHAPTER_ORDER_BY_LESSON_TYPE} from '../../shared/constants';
import chapterRoutes from './chapterRoutes';
import {GRAY, RED, SUCCESS_GREEN} from '../../shared/styles/colours';
import {setUserSessionValues} from '../../redux/actions';
import {DragAndDropComponent} from './Components';

const {width} = Dimensions.get('window');

// styles
import styles from './style';

const LessonContentScreen = ({route, navigation}) => {
  const fullLesson = route.params || {};

  const {
    category = 'Video',
    postTheoreticalContent = {},
    postPracticalContent = {},
    contentVideoValues,
  } = fullLesson;
  const lessonOrder = CHAPTER_ORDER_BY_LESSON_TYPE[category];
  const lastTheoryChapters = category === 'Video' ? 2 : 1;

  // useState ------------------------------------------------
  const [adjustedLesson, setAdjustedLesson] = useState(null);
  const [currentChapterContent, setCurrentChapterContent] = useState(null);
  const [currentChapterName, setcurrentChapterName] = useState(lessonOrder[0]);
  const [currentStep, setCurrentStep] = useState(0);
  const [isTaskChecked, setTaskState] = useState(false);
  // const [, setIfTaskCompletedCorrectly] = useState(false);
  // ---------------------------------------------------------

  // useEffects ----------------------------------------------
  // A future Implementation
  useEffect(() => {
    // let initLesson = {...postTheoreticalContent};

    // postPracticalContent.length &&
    //   postPracticalContent.forEach((obj) => (initLesson[obj.name] = obj));

    // initLesson = Object.entries(initLesson)
    //   .filter(([key, obj]) => lessonOrder.includes(key))
    //   .sort(
    //     ([key1, a], [key2, b]) =>
    //       lessonOrder.indexOf(key1) - lessonOrder.indexOf(key2),
    //   );

    // setAdjustedLesson(initLesson);
    // setcurrentChapterName(initLesson[0][0]);
    // setCurrentChapterContent(initLesson[0][1]);

    // A temprorary implementation
    let initLesson = {...postTheoreticalContent};

    initLesson = Object.entries(initLesson)
      .filter(([key, obj]) => lessonOrder.includes(key))
      .sort(
        ([key1, a], [key2, b]) =>
          lessonOrder.indexOf(key1) - lessonOrder.indexOf(key2),
      );

    postPracticalContent.length &&
      postPracticalContent.forEach(({name, content}) => {
        if (name === 'Another way to say') {
          content.length > 0 &&
            content.forEach(({answer, sentence}) => {
              // remove words in round brackets and remove only curly braces
              const modifiedSentence = sentence
                .replace(/ \([\s\S]*?\)/g, '')
                .replace('{', '')
                .replace('}', '')
                .split(' ')
                .map((word, key) => ({
                  id: key + 1,
                  word,
                }));

              initLesson.splice(initLesson.length - lastTheoryChapters, 0, [
                answer,
                modifiedSentence,
              ]);
            });
        }
      });
    if (initLesson.length) {
      setAdjustedLesson(initLesson);
      setcurrentChapterName(initLesson[0][0]);
      setCurrentChapterContent(initLesson[0][1]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // ---------------------------------------------------------

  const isTheory = Object.keys(postTheoreticalContent).includes(
    currentChapterName,
  );

  const MatchedComponent =
    chapterRoutes[currentChapterName] || DragAndDropComponent;

  const dispatch = useDispatch();

  return adjustedLesson && adjustedLesson.length ? (
    <View style={styles.container}>
      <View style={styles.progressBarContainer}>
        <ProgressBar
          color={SUCCESS_GREEN}
          width={width * 0.95}
          height={15}
          borderRadius={5}
          progress={(currentStep + 1) / adjustedLesson.length}
        />
      </View>
      <View style={styles.currentChapterNameContainer}>
        <Text style={styles.currentChapterNameText}>
          {isTheory
            ? currentChapterName
            : `Make a sentence with "${adjustedLesson[currentStep][0]}"`}
        </Text>
      </View>

      <View style={styles.currentChapterContentContainer}>
        <MatchedComponent
          currentChapterContent={currentChapterContent}
          isTaskChecked={isTaskChecked}
          {...(currentChapterName === 'Content' && contentVideoValues)}
        />
      </View>
      <View style={styles.bottomButtonContainer}>
        <Button
          style={[
            styles.bottomButton,
            {
              backgroundColor: isTheory || isTaskChecked ? SUCCESS_GREEN : GRAY,
            },
          ]}
          onPress={() => {
            if (currentStep < adjustedLesson.length - 1) {
              if (isTheory || isTaskChecked) {
                setCurrentStep(currentStep + 1);
                setcurrentChapterName(adjustedLesson[currentStep + 1][0]);
                setCurrentChapterContent(adjustedLesson[currentStep + 1][1]);
                setTaskState(false);
              } else {
                setTaskState(true);
              }
            } else {
              alert('Good job!');
              dispatch(
                setUserSessionValues({
                  activeLesson: '',
                }),
              );
              navigation.navigate(category);
            }
          }}>
          <Text style={styles.bottomButtonText}>
            {adjustedLesson && adjustedLesson.length - 1 === currentStep
              ? 'Finish up'
              : isTheory || (!isTheory && isTaskChecked)
              ? 'Continue'
              : 'Check'}
          </Text>
        </Button>
      </View>
    </View>
  ) : null;
};

export default LessonContentScreen;
