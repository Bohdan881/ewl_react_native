import {StyleSheet} from 'react-native';
import {WHITE} from '../../shared/styles/colours';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    paddingBottom: 15,
    paddingTop: 15,
    backgroundColor: WHITE,
  },
  scrollView: {
    flex: 1,
    height: '100%',
  },
  chapterContainer: {
    height: '100%',
  },
  progressBarContainer: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  currentChapterNameContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
  },
  currentChapterNameText: {
    fontSize: 20,
    fontWeight: '600',
  },
  currentChapterContentContainer: {
    flex: 12,
    width: '100%',
  },
  bottomButtonContainer: {
    flex: 1,
    width: '100%',
    marginBottom: 20,
  },
  bottomButton: {
    alignSelf: 'center',
    height: '100%',
    width: '100%',
    borderRadius: 5,
    justifyContent: 'center',
  },
  bottomButtonText: {
    fontWeight: '600',
    color: WHITE,
  },
});
