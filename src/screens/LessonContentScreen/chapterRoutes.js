import {
  AboutChapter,
  ContentChapter,
  DragAndDropComponent,
  ContentListChapter,
  SpeakingChapter,
} from './Components';

const chapterRoutes = {
  About: AboutChapter,
  'Words in context': DragAndDropComponent,
  'Fill in the blanks': DragAndDropComponent,
  Definitions: DragAndDropComponent,
  'Another way to say': DragAndDropComponent,
  Cloze: DragAndDropComponent,
  Content: ContentChapter,
  Speaking: SpeakingChapter,
  'Phrasal Verbs': ContentListChapter,
  'New Words': ContentListChapter,
};

export default chapterRoutes;
