import {StyleSheet, Dimensions} from 'react-native';
import {RED, WHITE} from '../../../../shared/styles/colours';
const {width} = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 13,
  },
  titleText: {
    lineHeight: 24,
    fontWeight: 'bold',
  },
  answersContainer: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: 20,
  },
  answerListContainer: {
    flex: 1,
    marginBottom: 15,
  },

  itemContent: {
    padding: 10,
    fontSize: 18,
  },
  correctSentenceSegment: {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    height: 145,
    paddingTop: 10,
    paddingLeft: 10,
    bottom: -55,
    width: width,
    left: -10,
  },
  correctSentenceHeader: {
    fontWeight: '700',
    fontSize: 20,
    textAlign: 'left',
    paddingBottom: 5,
  },
});
