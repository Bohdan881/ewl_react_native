import React, {useState, useEffect} from 'react';
import {View, Text, Spinner, Segment} from 'native-base';
import {AnswerList} from './components';
import AnswerWord from './components/AnswerWord';
import {isArrayEqual, shuffleArray} from '../../../../utils';

// styles
import styles from './style';

const DragAndDropComponent = ({currentChapterContent, isTaskChecked}) => {
  const [content, setContent] = useState([]);
  const [correctSentence, setCorrectSentence] = useState('');
  const [userSolution, setUserSolution] = useState([]);

  useEffect(() => {
    let initSentence = '';

    currentChapterContent.length &&
      currentChapterContent.forEach(({word}) => (initSentence += `${word} `));

    setCorrectSentence(initSentence);
    setContent(shuffleArray(currentChapterContent));
  }, [currentChapterContent]);

  // const onCalculateCurrentSolution = () => {
  //   let currentSolution = '';

  //   userSolution.length &&
  //     userSolution
  //       .filter((obj) => obj.order.value !== -1)
  //       .sort((a, b) =>
  //         a.y.value === b.y.value
  //           ? a.x.value - b.x.value
  //           : a.y.value - b.y.value,
  //       )
  //       .forEach((obj) => {
  //         if (obj.word) {
  //           currentSolution += ` ${obj.word}`;
  //         }
  //       });
  //   return currentSolution;
  // };

  return content &&
    content.length &&
    // content[0].word === currentChapterContent[0].word
    isArrayEqual(content, currentChapterContent) ? (
    <>
      <View style={[styles.answerListContainer]}>
        <AnswerList
          userSolution={userSolution}
          setUserSolution={setUserSolution}>
          {content.length > 0 &&
            content.map((obj) => <AnswerWord key={obj.word} {...obj} />)}
        </AnswerList>
      </View>
      {isTaskChecked && (
        <Segment style={[styles.correctSentenceSegment]}>
          <Text style={[styles.correctSentenceHeader]}>Correct solution: </Text>
          <Text>{correctSentence}</Text>
        </Segment>
      )}
    </>
  ) : (
    <View>
      <Spinner />
    </View>
  );
};

// flat list will be used in the future
/* <FlatList
        data={content}
        renderItem={({item}) => {
          const {id, sentence} = item;
          return (
            <View key={`${id}`} style={styles.itemContainer}>
              <Text style={styles.itemContent}>
                {id + 1}. {sentence}
              </Text>
            </View>
          );
        }}
        keyExtractor={(item) => item.answer}
      /> */

export default DragAndDropComponent;
