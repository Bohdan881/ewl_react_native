import React from 'react';
import {View, StyleSheet} from 'react-native';
import {PanGestureHandler} from 'react-native-gesture-handler';
import Animated, {
  runOnJS,
  runOnUI,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import {between, useVector} from 'react-native-redash';
import {move} from 'react-native-redash';
import {SELAGO} from '../../../../../../shared/styles/colours';
import {isArrayEqual} from '../../../../../../utils';

const MARGIN_TOP = 250;
const MARGIN_LEFT = 32;
const WORD_HEIGHT = 55;
const NUMBER_OF_LINES = 4;
const SENTENCE_HEIGHT = (NUMBER_OF_LINES - 1) * WORD_HEIGHT;

const isNotInBank = (offset) => {
  'worklet';
  return offset.order.value !== -1;
};

const byOrder = (a, b) => {
  'worklet';
  return a.order.value > b.order.value ? 1 : -1;
};

const reorder = (input, from, to) => {
  'worklet';
  const offsets = input.filter(isNotInBank).sort(byOrder);
  const newOffset = move(offsets, from, to);
  newOffset.map((offset, index) => (offset.order.value = index));
};

const calculateLayout = (input, containerWidth) => {
  'worklet';
  const offsets = input.filter(isNotInBank).sort(byOrder);

  if (offsets.length === 0) {
    return;
  }

  let lineNumber = 0;
  let lineBreak = 0;

  offsets.forEach((offset, index) => {
    const total = offsets
      .slice(lineBreak, index)
      .reduce((acc, o) => acc + o.width.value, 0);

    if (total + offset.width.value > containerWidth) {
      lineNumber += 1;
      lineBreak = index;
      offset.x.value = 0;
    } else {
      offset.x.value = total;
    }

    offset.y.value = WORD_HEIGHT * lineNumber;
  });
};

const lastOrder = (input) => {
  'worklet';
  return input.filter(isNotInBank).length;
};

export const remove = (input, index) => {
  'worklet';
  const offsets = input
    .filter((o, i) => i !== index)
    .filter(isNotInBank)
    .sort(byOrder);

  offsets.map((offset, i) => (offset.order.value = i));
};

const SortableAnswerWord = ({
  offsets = [],
  index = 0,
  children,
  containerWidth,
  setUserSolution,
  userSolution = [],
}) => {
  const offset = offsets[index];
  const isGestureActive = useSharedValue(false);
  const isAnimating = useSharedValue(false);
  const translation = useVector();
  const isInBank = useDerivedValue(() => offset.order.value === -1);

  const onGestureEvent = useAnimatedGestureHandler({
    onStart: (_, ctx) => {
      if (isInBank.value) {
        translation.x.value = offset.originalX.value - MARGIN_LEFT;
        translation.y.value = offset.originalY.value + MARGIN_TOP;
      } else {
        translation.x.value = offset.x.value;
        translation.y.value = offset.y.value;
      }
      ctx.x = translation.x.value;
      ctx.y = translation.y.value;
      isGestureActive.value = true;
    },
    onActive: ({translationX, translationY}, ctx) => {
      const contextX = ctx.x || 0;
      const contextY = ctx.y || 0;

      translation.x.value = contextX + translationX;
      translation.y.value = contextY + translationY;

      if (isInBank.value && translation.y.value < SENTENCE_HEIGHT) {
        offset.order.value = lastOrder(offsets);
        calculateLayout(offsets, containerWidth);
      } else if (!isInBank.value && translation.y.value > SENTENCE_HEIGHT) {
        offset.order.value = -1;
        remove(offsets, index);
        calculateLayout(offsets, containerWidth);
      }

      // calculate boxes when they are between boxes
      for (let i = 0; i < offsets.length; i++) {
        if (i === index) {
          continue;
        }

        const o = offsets[i];
        if (
          between(translation.x.value, o.x.value, o.x.value + o.width.value) &&
          between(translation.y.value, o.y.value, o.y.value + WORD_HEIGHT)
        ) {
          reorder(offsets, offset.order.value, o.order.value);
          calculateLayout(offsets, containerWidth);
          break;
        }
      }
    },
    onEnd: () => {
      isGestureActive.value = false;
      translation.x.value = withSpring(offset.x.value);
      translation.y.value = withSpring(offset.y.value);

      runOnJS(setUserSolution)(offsets);
    },
  });

  const translateX = useDerivedValue(() => {
    if (isGestureActive.value) {
      return translation.x.value;
    }
    return withSpring(
      isInBank.value ? offset.originalX.value - MARGIN_LEFT : offset.x.value,
    );
  });

  const translateY = useDerivedValue(() => {
    if (isGestureActive.value) {
      return translation.y.value;
    }

    return withSpring(
      isInBank.value ? offset.originalY.value + MARGIN_TOP : offset.y.value,
    );
  });

  const style = useAnimatedStyle(() => {
    return {
      position: 'absolute',
      top: 0,
      left: 0,
      zIndex: isGestureActive.value || isAnimating.value ? 100 : 0,
      width: offset.width.value,
      height: WORD_HEIGHT,
      transform: [
        {translateX: translateX.value},
        {translateY: translateY.value},
      ],
    };
  });

  return children ? (
    <>
      <View
        // eslint-disable-next-line react-native/no-inline-styles
        style={{
          backgroundColor: SELAGO,
          position: 'absolute',
          top: offset.originalY.value + MARGIN_TOP,
          left: offset.originalX.value - MARGIN_LEFT,
          /* height: offset.height.value - 4, */
          height: WORD_HEIGHT - 8,
          width: offset.width.value - 4,
          borderRadius: 8,
        }}
      />

      <Animated.View style={style}>
        <PanGestureHandler onGestureEvent={onGestureEvent}>
          <Animated.View style={StyleSheet.absoluteFill}>
            {children}
          </Animated.View>
        </PanGestureHandler>
      </Animated.View>
    </>
  ) : null;
};

export default SortableAnswerWord;
