/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/rules-of-hooks */
import React, {useState} from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import {View} from 'native-base';
import SortableAnswerWord from '../SortableAnswerWord';
import {useSharedValue, runOnUI, runOnJS} from 'react-native-reanimated';

// styles
import {SELAGO} from '../../../../../../shared/styles/colours';

const MARGIN = 32;
const containerWidth = Dimensions.get('window').width - 32 * 2;

const AnswerList = ({children, userSolution, setUserSolution}) => {
  const [ready, setReady] = useState(false);

  const offsets = children.length
    ? children.map(() => {
        return {
          order: useSharedValue(0),
          width: useSharedValue(0),
          height: useSharedValue(0),
          x: useSharedValue(0),
          y: useSharedValue(0),
          originalX: useSharedValue(0),
          originalY: useSharedValue(0),
          word: useSharedValue(0),
        };
      })
    : [];

  if (offsets.length > 0) {
    if (!ready) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'center',
            opacity: 0,
          }}>
          {children.length &&
            children.map((child, index) => {
              let word = '';
              Object.values(child).forEach((obj) => {
                if (obj && obj.word) {
                  word = obj.word;
                }
              });
              return (
                <View
                  key={index}
                  onLayout={(event) => {
                    const {width, height, x, y} = event.nativeEvent.layout;
                    const offset = offsets[index];
                    offset.word.value = word;
                    offset.order.value = -1;
                    offset.width.value = width;
                    offset.height.value = height;
                    offset.originalX.value = x;
                    offset.originalY.value = y;

                    runOnUI(() => {
                      'worklet';
                      if (
                        offsets.filter((o) => o.order.value !== -1).length === 0
                      ) {
                        runOnJS(setReady)(true);
                      }
                    })();
                  }}>
                  {child}
                </View>
              );
            })}
        </View>
      );
    }

    return (
      <View style={[{margin: MARGIN}]}>
        <View style={[StyleSheet.absoluteFill]}>
          {[0, 50, 50 * 2, 50 * 3.1].map((line) => {
            return (
              <View
                key={line}
                style={{
                  top: line - 2,
                  width: '100%',
                  height: 2,
                  backgroundColor: SELAGO,
                }}
              />
            );
          })}
        </View>
        {children.length &&
          children.map((child, index) => {
            return (
              <SortableAnswerWord
                key={index}
                offsets={offsets || []}
                index={index}
                userSolution={userSolution}
                setUserSolution={setUserSolution}
                containerWidth={containerWidth}>
                {child}
              </SortableAnswerWord>
            );
          })}
      </View>
    );
  } else {
    return null;
  }
};

export default AnswerList;
