import React from 'react';
import {View, Text} from 'native-base';

// style
import styles from './style';

const AnswerWord = ({word}) => {
  return (
    <View style={styles.root}>
      <View>
        <View style={styles.container}>
          <Text style={styles.text}>{word}</Text>
        </View>
        <View style={styles.shadow} />
      </View>
    </View>
  );
};

export default AnswerWord;
