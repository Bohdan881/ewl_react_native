import {StyleSheet} from 'react-native';
import {WHITE, SELAGO} from '../../../../../../shared/styles/colours';

export default styles = StyleSheet.create({
  root: {
    padding: 4,
  },
  container: {
    padding: 8,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: SELAGO,
    backgroundColor: WHITE,
  },
  text: {
    fontSize: 19,
  },
  shadow: {
    ...StyleSheet.absoluteFillObject,
    borderRadius: 8,
    borderBottomWidth: 3,
    borderColor: SELAGO,
    top: 4,
  },
});
