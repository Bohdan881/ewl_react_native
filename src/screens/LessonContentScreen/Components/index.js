import AboutChapter from './AboutChapter';
import DragAndDropComponent from './DragAndDropComponent/DragAndDropComponent';
import ContentChapter from './ContentChapter';
import SpeakingChapter from './SpeakingChapter';
import ContentListChapter from './ContentListChapter';

export {
  AboutChapter,
  DragAndDropComponent,
  ContentChapter,
  SpeakingChapter,
  ContentListChapter,
};
