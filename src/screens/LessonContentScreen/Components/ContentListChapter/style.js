import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 15,
  },
  scrollView: {flex: 1, height: '100%'},
  phrasalVerbsContiner: {
    flexDirection: 'column',
  },
  wordHeader: {
    fontSize: 20,
    paddingTop: 10,
    paddingBottom: 5,
    fontWeight: '600',
  },
  examplesHeader: {
    position: 'relative',
    fontSize: 18,
    fontWeight: '600',
    paddingTop: 5,
    paddingBottom: 5,
    left: -5,
  },
  structureContainer: {
    flexDirection: 'row',
  },
});
