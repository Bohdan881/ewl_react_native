import React from 'react';
import {Text, View} from 'native-base';
import {useWindowDimensions} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import HTML from 'react-native-render-html';

// styles
import styles from './style';

const ContentListChapter = ({currentChapterContent = []}) => {
  const contentWidth = useWindowDimensions().width;
  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView style={styles.scrollView}>
        <View style={styles.phrasalVerbsContiner}>
          {currentChapterContent.length &&
            currentChapterContent.map(
              ({id, description, header, examples = [], structures = []}) => {
                return (
                  <View key={id} style={styles.phrasalVerbContainer}>
                    <Text style={styles.wordHeader}>
                      {id}. {header}
                    </Text>
                    <HTML
                      tagsStyles={{p: {fontSize: 60}}}
                      source={{html: description}}
                      contentWidth={contentWidth}
                    />
                    {examples && examples.length > 0 && (
                      <>
                        <Text style={styles.examplesHeader}> Examples</Text>
                        {examples.map(({exampleId, example = ''}) => {
                          return (
                            <HTML
                              key={exampleId}
                              tagsStyles={{p: {lineHeight: 25}}}
                              source={{html: example}}
                              contentWidth={contentWidth}
                            />
                          );
                        })}
                      </>
                    )}
                    {/* {structures && structures.length > 0 && (
                      <>
                        <Text style={styles.examplesHeader}> Structures</Text>
                        {structures.map(
                          ({
                            structureId,
                            firstWord,
                            secondWord,
                            structureExample,
                          }) => {
                            return (
                              <Text
                                key={structureId}
                                style={styles.structureContainer}>
                                {firstWord} + {secondWord}
                                <HTML
                                  key={structureId}
                                  tagsStyles={{p: {lineHeight: 25}}}
                                  source={{html: structureExample}}
                                  contentWidth={contentWidth}
                                />
                              </Text>
                            );
                          },
                        )}
                      </>
                    )} */}
                  </View>
                );
              },
            )}
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default ContentListChapter;
