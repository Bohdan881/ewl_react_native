import React from 'react';
import {ScrollView} from 'react-native-gesture-handler';
import {Text, View} from 'native-base';

// styles
import styles from './style';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const SpeakingChapter = ({currentChapterContent}) => {
  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        style={styles.scrollView}
        keyboardShouldPersistTaps="always">
        <Text style={styles.headerText}>Discuss the following questions.</Text>
        <Text style={styles.descriptionText}>
          Try to give as much detail as you can. Elaborate with examples or
          stories whenever possible.
        </Text>
        <View style={styles.contentContainer}>
          {currentChapterContent.map(({contentBlocks = [], id}) => (
            <View key={id} style={styles.sentenceContainer}>
              {contentBlocks.length &&
                contentBlocks.map(({blockId, blockContent}) => (
                  <Text style={styles.sentenceText} key={blockId}>
                    {blockContent}
                  </Text>
                ))}
            </View>
          ))}
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default SpeakingChapter;
