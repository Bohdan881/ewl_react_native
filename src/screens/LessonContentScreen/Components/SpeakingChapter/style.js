import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 10,
  },
  headerText: {
    paddingBottom: 10,
  },
  descriptionText: {
    paddingBottom: 10,
  },
  contentContainer: {
    paddingTop: 10,
  },
  sentenceContainer: {
    paddingBottom: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  sentenceText: {
    lineHeight: 23,
  },
});
