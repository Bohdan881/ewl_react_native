import React from 'react';
import {useWindowDimensions} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {View} from 'native-base';
import HTML from 'react-native-render-html';
import {WebView} from 'react-native-webview';
const draftToHtml = require('draftjs-to-html');

// styles
import styles from './style';

const ContentChapter = ({currentChapterContent, id, link, source}) => {
  const parsedText =
    typeof JSON.parse(currentChapterContent) === 'string'
      ? JSON.parse(currentChapterContent)
      : draftToHtml(JSON.parse(currentChapterContent));
  const contentWidth = useWindowDimensions().width;

  return (
    <View style={styles.container}>
      <View style={styles.webViewContainer}>
        <WebView
          javaScriptEnabled={true}
          domStorageEnabled={true}
          allowsFullscreenVideo={false}
          source={{uri: `https://www.youtube.com/embed/${id}`}}
        />
      </View>
      <View>
        <HTML
          tagsStyles={{p: {fontSize: 18, lineHeight: 25, paddingTop: 15}}}
          source={{html: parsedText}}
          contentWidth={contentWidth}
        />
      </View>
    </View>
  );
};

export default ContentChapter;
