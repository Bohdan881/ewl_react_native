import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  webViewContainer: {
    height: 250,
    marginBottom: 15,
  },
});
