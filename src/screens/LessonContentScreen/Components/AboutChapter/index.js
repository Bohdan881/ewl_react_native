import {View} from 'native-base';
import React from 'react';
import {useWindowDimensions} from 'react-native';
import HTML from 'react-native-render-html';
const draftToHtml = require('draftjs-to-html');

// styles
import styles from './style';

const AboutChapter = ({currentChapterContent}) => {
  const parsedText =
    typeof JSON.parse(currentChapterContent) === 'string'
      ? JSON.parse(currentChapterContent)
      : draftToHtml(JSON.parse(currentChapterContent));

  const contentWidth = useWindowDimensions().width;

  return (
    <View style={styles.container}>
      <HTML
        tagsStyles={{p: {lineHeight: 25}}}
        source={{html: parsedText}}
        contentWidth={contentWidth}
        alterChildren={(node) => {
          const {children, name} = node;
          const specialCharacters = /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;

          if (name === 'em' && children && children.length) {
            const data = children[0].data;
            const modifiedChildren = data.match(specialCharacters)
              ? `${data} `
              : ` ${data}`;

            return [{...children[0], data: modifiedChildren}];
          }
        }}
      />
    </View>
  );
};

export default AboutChapter;
