import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
  CORN_FLOWER_BLUE,
  DARK_GREY,
  FACEBOOK_COLOUR,
  RED,
  LIGHT_GREY,
  WHITE,
} from '../../shared/styles/colours';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    margin: width * 0.05,
    paddingTop: height < 700 ? 0 : hp('10%'),
  },
  scrollView: {
    flex: 1,
    width: '100%',
  },
  logo: {
    height: wp('25%'),
    width: wp('25%'),
    alignSelf: 'center',
    margin: 20,
  },
  input: {
    height: width * 0.1,
    borderRadius: 5,
    overflow: 'hidden',
    backgroundColor: WHITE,
    marginTop: 10,
    marginBottom: 10,
    paddingLeft: 16,
  },
  button: {
    backgroundColor: CORN_FLOWER_BLUE,
    marginTop: 10,
    height: width * 0.11,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonTitle: {
    color: WHITE,
    fontSize: 16,
    fontWeight: 'bold',
  },
  footerView: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20,
  },
  footerText: {
    marginTop: width * 0.1,
    fontSize: 16,
    color: DARK_GREY,
  },
  forgotPasswordLink: {
    color: CORN_FLOWER_BLUE,
    fontWeight: '600',
    fontSize: 16,
    paddingBottom: 10,
  },
  otherAccountsWrapper: {
    marginTop: 10,
    marginBottom: 20,
  },
  footerLineView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
  },
  footerLine: {
    flex: 1,
    height: 1,
    backgroundColor: LIGHT_GREY,
  },
  footerLineText: {
    width: 50,
    textAlign: 'center',
  },
  signInAnotherAccount: {
    display: 'flex',
    flexDirection: 'row',
  },
  anotherAccountIcon: {
    width: 20,
    height: 20,
  },
  buttonWrapper: {
    marginTop: 20,
    width: 190,
  },
  facebookButton: {
    backgroundColor: FACEBOOK_COLOUR,
  },
  googleButton: {
    backgroundColor: RED,
  },
  footerLink: {
    color: CORN_FLOWER_BLUE,
    fontWeight: 'bold',
    fontSize: 16,
  },
});
