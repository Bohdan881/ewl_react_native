import React, {useState} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import {firebase} from '../../firebase/config';
import {setUserSessionValues} from '../../redux/actions';
import {GRAY} from '../../shared/styles/colours';

// styles
import style from './style';

const LoginScreen = ({navigation}) => {
  // states -----------------------------------
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  // -----------------------------------------

  const onFooterLinkPress = () => {
    navigation.navigate('Registration');
  };

  const onLoginPress = () => {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((response) => {
        const uid = response.user.uid;

        // fetch user data by uid
        firebase
          .database()
          .ref(`users/${uid}`)
          .once('value')
          .then((snapshot) => {
            const {
              emailVerified,
              isAgreedOnCookies,
              username,
              lessonsInProgress,
              lessonsCompleted,
            } = snapshot.val() || {};

            setUserSessionValues({
              uid,
              email,
              userName: username,
              isEmailVerified: emailVerified,
              isAgreedOnCookies,
              lessonsInProgress,
              lessonsCompleted,
            });

            navigation.navigate('Main');
          });
      })
      .catch((error) => alert(error));
  };

  const onLoginWithFacebook = () => {
    // TO DO
    // connect fo facebook sdk
  };

  const onLoginWithGoogle = () => {
    // TO DO
    // connect fo google sdk
  };

  return (
    <View style={style.container}>
      <KeyboardAwareScrollView
        style={style.scrollView}
        keyboardShouldPersistTaps="always">
        <Image
          style={style.logo}
          source={require('../../assets/default.png')}
        />

        <TextInput
          style={style.input}
          placeholder="E-mail"
          placeholderTextColor={GRAY}
          onChangeText={(text) => setEmail(text)}
          value={email}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TextInput
          style={style.input}
          placeholderTextColor={GRAY}
          secureTextEntry
          placeholder="Password"
          onChangeText={(text) => setPassword(text)}
          value={password}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TouchableOpacity style={style.button} onPress={() => onLoginPress()}>
          <Text style={style.buttonTitle}>Sign in</Text>
        </TouchableOpacity>
        <View style={style.footerView}>
          <Text
            style={style.forgotPasswordLink}
            onPress={() => navigation.navigate('Forget Password')}>
            Forgot password?
          </Text>

          <View style={style.footerLineView}>
            <View style={style.footerLine} />
            <View>
              <Text style={style.footerLineText}>OR</Text>
            </View>
            <View style={style.footerLine} />
          </View>
          <View style={style.buttonWrapper}>
            <Icon.Button
              name="facebook"
              style={style.facebookButton}
              onPress={() => onLoginWithFacebook()}>
              Login with Facebook
            </Icon.Button>
          </View>
          <View style={style.buttonWrapper}>
            <Icon.Button
              name="google"
              style={style.googleButton}
              onPress={() => onLoginWithGoogle()}>
              Login with Google
            </Icon.Button>
          </View>
          <Text style={style.footerText}>
            Don't have an account?{' '}
            <Text onPress={onFooterLinkPress} style={style.footerLink}>
              Sign up
            </Text>
          </Text>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default LoginScreen;
