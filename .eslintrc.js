module.exports = {
  root: true,
  extends: '@react-native-community',
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  rules: {
    'no-alert': 'off',
  },
};
